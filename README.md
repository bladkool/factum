# factum
A small Kemal test program to show a message in the browser.

**Crystal**
- run ```shards install``` to resolve and install dependencies
- run ```crystal run src/web.cr```
- wait for the message ```[development] Kemal is ready to lead at http://0.0.0.0:3000```
- open a browser and goto http://localhost:3000
- the message 'Kemal says, Hello World!' is shown in the browser

**Docker**
- run ```docker build -t kemal-test .``` (note te dot at the end)
- run ```docker run --detach -p 3000:3000 kemal-test```
- run ```docker ps``` and copy the container id of image kemal-test
- run ```docker logs <container id>``` and check for message ```[development] Kemal is ready to lead at http://0.0.0.0:3000```
- open a browser and goto http://localhost:3000
- the message 'Kemal says, Hello World!' is shown in the browser
- enter ```docker stop <container id>``` to stop docker

**Docker compose**
- run ```docker-compose up -d --build``` if run for the first time, otherwise run ```docker-compose up -d```
- run ```docker-compose logs``` and check for message ```[development] Kemal is ready to lead at http://0.0.0.0:3000```
- open a browser and goto http://localhost:3000
- the message 'Kemal says, Hello World!' is shown in the browser
- enter ```docker-compose down``` to stop docker