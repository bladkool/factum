FROM crystallang/crystal

# Add this directory to container as /app
ADD . /app
WORKDIR /app

# Install dependencies
RUN shards install

# Build app
RUN crystal build --release src/web.cr -o ./test

# Run tests
# RUN crystal spec

EXPOSE 3000

CMD ./test