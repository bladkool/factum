require "kemal"

module Web
  VERSION = "0.1.0"

  # Matches GET "http://host:port/"
  get "/" do
    "Kemal says, Hello World!"
  end

  Kemal.run
end